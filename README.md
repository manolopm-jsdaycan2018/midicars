# Midi Cars

Todas las imágenes de este proyecto son propiedad de KENNEY  (https://www.kenney.nl) 
y aunqué están públicadas como creative common zero, veo justo atribuirle todo el merito. 

Yo las compré, si quieres usarlas, dale soporte y compralas también.

Este es un proyecto de juguete que preparé para la JSDayCAN2018

Usa webmidi y react para interactuar con un WordDE Easycontrol

La idea es demostrar que con un mixer midi se puede interactuar con juegos de una forma más natural.

Para probarlo está en linea aquí: https://www.graph-ic.org/react-experiments/midi-demos/midicars/public/

Para usarlo en local:
- npm run build

