'use strict'
import React from 'react'

export default class IntGreenUpLeft extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-148 -148.1)">
        <path d="m148 176.1v-28h28q0.55 28.35-28 28" fill="#27ae60"/>
        <path d="m180 148.1h1.95q0.7 34.45-33.95 34v-2q32.6 0.4 32-32" fill="#b17940"/>
        <path d="m181.95 148.1h30.05v64h-64v-30q34.65 0.45 33.95-34m1.35 38.75-6.55-0.8-0.75 6.6 6.55 0.75 0.75-6.55m-3.25 9.35-0.4 3.15 3.15 0.4 0.45-3.1-3.2-0.45m12.05-1.5-1.2-4.4-4.4 1.2 1.2 4.45 4.4-1.25" fill="#bb8044"/>
        <path d="m148 176.1q28.55 0.35 28-28h4q0.6 32.4-32 32v-4" fill="#2fc06c"/>
        <path d="m183.3 186.85-0.75 6.55-6.55-0.75 0.75-6.6 6.55 0.8m8.8 7.85-4.4 1.25-1.2-4.45 4.4-1.2 1.2 4.4" fill="#aa753e"/>
        <path d="m180.05 196.2 3.2 0.45-0.45 3.1-3.15-0.4 0.4-3.15" fill="#c68a4e"/>
        </g>
        </svg>
    )
  }
}
