'use strict'
import React from 'react'

export default class IntBrownUpLeft extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <defs>
        <linearGradient id="gradient6" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0065308 .0065613 0 343.95 98.45)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>
        <linearGradient id="gradient7" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0078735 .0080566 0 331.05 112.7)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>
        </defs>
        <g transform="translate(-296 -74.1)">
        <path d="m296 106.1v-2.05q30.15 1.35 30-29.95h2q0.15 33.45-32 32" fill="#b17940"/>
        <path d="m296 106.1q32.15 1.45 32-32h4q0.15 37.55-36 35.9v-3.9" fill="#2fc06c"/>
        <path d="m332 74.1h28v64h-64v-28.1q36.15 1.65 36-35.9m12.45 19.5-0.25-0.4-0.5-0.15-0.45 0.2-0.25 0.4-1.35 6.75-2.05-0.65-0.5 0.05-0.4 0.3v0.75l1.15 2.95h7.95l1.5-5.05-0.05-0.55v-0.05l-0.4-0.3q-0.3-0.15-0.55 0l-2.25 0.9-1.6-5.15m-7.75 15.8-4.65 1.65-3.25-4.65-0.4-0.25-0.4-0.05-0.4 0.25q-0.15 0.15-0.15 0.4l-0.65 5.75-2.75 0.55-0.4 0.2-0.1 0.15-0.1 0.3q-0.05 0.2 0.05 0.4l0.05 0.05 2.4 5.15h9.8l2.65-1.75 0.2-0.25 0.1-0.2-0.1-0.55q-0.15-0.25-0.45-0.35l-2.5-0.7 2-5.15v-0.55l-0.05-0.1-0.35-0.3q-0.25-0.15-0.55 0" fill="#27ae60"/>
        <path d="m296 104.05v-29.95h30q0.15 31.3-30 29.95" fill="#bb8044"/>
        <path d="m344.45 93.6 1.6 5.15 2.25-0.9q0.25-0.15 0.55 0l0.4 0.3v0.05l0.05 0.55-1.5 5.05h-7.95l-1.15-2.95v-0.75l0.4-0.3 0.5-0.05 2.05 0.65 1.35-6.75 0.25-0.4 0.45-0.2 0.5 0.15 0.25 0.4" fill="url(#gradient6)"/>
        <path d="m336.7 109.4q0.3-0.15 0.55 0l0.35 0.3 0.05 0.1v0.55l-2 5.15 2.5 0.7q0.3 0.1 0.45 0.35l0.1 0.55-0.1 0.2-0.2 0.25-2.65 1.75h-9.8l-2.4-5.15-0.05-0.05q-0.1-0.2-0.05-0.4l0.1-0.3 0.1-0.15 0.4-0.2 2.75-0.55 0.65-5.75q0-0.25 0.15-0.4l0.4-0.25 0.4 0.05 0.4 0.25 3.25 4.65 4.65-1.65" fill="url(#gradient7)"/>
        </g>
        </svg>
    )
  }
}
