'use strict'
import React from 'react'

export default class IntGreenDown extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <defs>
        <linearGradient id="gradient1" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0065308 .0065613 0 83.15 48.75)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>
        </defs>
        <g transform="translate(-74 -0.1)">
        <path d="m74 32.1v-2q3.95 3 7.9 3l3.15-0.4q2.45-0.7 4.95-2.6 8-6.05 16 0t16 0 16 0v2q-8-6.05-16 0t-16 0-16 0-16 0" fill="#b17940"/>
        <path d="m74 32.1q8 6.05 16 0t16 0 16 0 16 0v4q-8-6.05-16 0t-16 0-16 0-16 0v-4" fill="#2fc06c"/>
        <path d="m74 30.1v-30h64v30q-8-6.05-16 0t-16 0-16 0q-2.5 1.9-4.95 2.6l-3.15 0.4q-3.95 0-7.9-3m41.25-13.75-6.55 2.9 2.75 6.5 6.6-2.85-2.8-6.55" fill="#bb8044"/>
        <path d="m138 36.1v28h-64v-28q8 6.05 16 0t16 0 16 0 16 0m-55.55 7.45-0.25 0.4-1.35 6.75-2.05-0.65-0.5 0.05-0.4 0.3v0.75l1.15 2.95h7.95l1.5-5.05-0.05-0.55v-0.05l-0.4-0.3q-0.3-0.15-0.55 0l-2.25 0.9-1.6-5.15-0.25-0.4q-0.25-0.15-0.5-0.15l-0.45 0.2" fill="#27ae60"/>
        <path d="m82.45 43.55 0.45-0.2q0.25 0 0.5 0.15l0.25 0.4 1.6 5.15 2.25-0.9q0.25-0.15 0.55 0l0.4 0.3v0.05l0.05 0.55-1.5 5.05h-7.95l-1.15-2.95v-0.75l0.4-0.3 0.5-0.05 2.05 0.65 1.35-6.75 0.25-0.4" fill="url(#gradient1)"/>
        <path d="m115.25 16.35 2.8 6.55-6.6 2.85-2.75-6.5 6.55-2.9" fill="#aa753e"/>
        </g>
        </svg>
    )
  }
}
