'use strict'
import React from 'react'
import Car from './car'
import Track from './track'

export default class Game extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      error:'',
      inputs:'',
      outputs:'',
      cars:[
        { angle: 0, speed: 0, x:  74, y: 152, },
        { angle: 0, speed: 0, x: 128, y: 192, },
        { angle: 0, speed: 0, x:  74, y: 216, },
        { angle: 0, speed: 0, x: 128, y: 256, },
        { angle: 0, speed: 0, x:  74, y: 280, },
        { angle: 0, speed: 0, x: 128, y: 320, },
        { angle: 0, speed: 0, x:  74, y: 344, },
        { angle: 0, speed: 0, x: 128, y: 384, },
        { angle: 0, speed: 0, x:  74, y: 408, }
      ]}
    this.messageIn = this.messageIn.bind(this)
    this.update = this.update.bind(this)
    setTimeout(this.update,33)
  }

  componentDidMount () {
    navigator.requestMIDIAccess().then(
      midi => {
        let deviceIdIn = ''
        let deviceIdOut = ''
        
        midi.inputs.forEach(device => {
          if (device.name === 'WORLDE easy control MIDI 1') {
            deviceIdIn = device.id

            midi.inputs
              .get(deviceIdIn)
              .onmidimessage = this.messageIn
          }
        })
        
        midi.outputs.forEach(device => {
          if (device.name === 'WORLDE easy control MIDI 1') deviceIdOut = device.id
        })


        this.setState({
          error: '',
          inputs: midi.inputs.get(deviceIdIn),
          outputs: midi.outputs.get(deviceIdOut),
          cars: this.state.cars
        })
      },
      error =>
        this.setState({error: error, inputs: '', outputs: '', cars: this.state.cars})
    )
    
  }

  update () {
    let cars = this.state.cars.map((car,i) => {
      let tmpCar = car
      let angle = tmpCar.angle%360
      let angle2 = tmpCar.angle * (Math.PI / 180)
//      if (i===0) console.log(tmpCar.angle, angle, Math.asin(angle), Math.asin(angle), tmpCar.speed)
      tmpCar.x -= Math.sin(angle2)*tmpCar.speed
      tmpCar.y += Math.cos(angle2)*tmpCar.speed
      return tmpCar
    })

    this.setState(Object.assign(this.state,{cars: cars}),()=>setTimeout(this.update,33))
  }

  messageIn (message, err) {
    if (message.data[0]===176) {
      let cars = this.state.cars

      if ((message.data[1]>=3) && (message.data[1]<=11)) 
        cars[message.data[1]-3].speed = -message.data[2]/127

      if ((message.data[1]>=14) && (message.data[1]<=22)) {
        cars[message.data[1]-14].angle = (message.data[2]/127)*720
        let q = ((message.data[2]/127)*720)%360
        if ((q>=270) && (q<=360)) q-=360
        console.log((message.data[2]/127)*720, q)
      }
     
      this.setState(Object.assign(this.state,{cars: cars}))
    }
  }
  
  render () {
    return ([
        <Track key={'track'}/>,
      this.state.cars.map(
        (car,i) => 
          <Car key={'w'+i} status={car} style={{minWidth: '50px',maxWidth: '50px',position: 'absolute',left: car.x,top: car.y}} />
      )
    ])
  }
}
  
