'use strict'
import React from 'react'

export default class TrackTop extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-248 -26)">
        <path d="m296 28v-2h16v2h-16m-16-2v2h-16v-2h16m16 8h16v2h-16v-2m-16 0v2h-16v-2h16" fill="#eee"/>
        <path d="m280 34h16v2h-16v-2m16-8v2h-16v-2h16m-48 10v-2h16v2h-16m0-8v-2h16v2h-16" fill="#db6212"/>
        <path d="m248 28h16v6h-16v-6m32 0h16v6h-16v-6" fill="#e86a17"/>
        <path d="m280 28v6h-16v-6h16m16 0h16v6h-16v-6" fill="#fafafa"/>
        <path d="m296 36h16v5h-64v-5h48" fill="#9cc0c2"/>
        <path d="m312 41v49h-64v-49h64" fill="#a6c9cb"/>
        </g>
        </svg>
    )
  }
}
