'use strict'
import React from 'react'

export default class GoalRight extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-1210 -248)">

        <path d="m1272 248v16h-6v-16h6m-6 32h6v16h-6v-16" fill="#e86a17"/>
        <path d="m1266 280v16h-2v-16h2m6-32h2v16h-2v-16m2 32v16h-2v-16h2m-10-32h2v16h-2v-16" fill="#db6212"/>
        <path d="m1264 248v64h-5v-64h5" fill="#9cc0c2"/>
        <path d="m1272 312h-6v-16h6v16m-6-48h6v16h-6v-16" fill="#fafafa"/>
        <path d="m1266 264v16h-2v-16h2m6 0h2v16h-2v-16m2 32v16h-2v-16h2m-8 16h-2v-16h2v16" fill="#eee"/>
        <path d="m1259 312h-49v-64h49v64" fill="#a6c9cb"/>
        <path d="m1253 269v11h9v10h-9v-10h-11v10h-11v-10h-10v10h-11v-10h11v-11h10v11h11v-11h11m9-4v2h-52v-2h52m0 27v2h-52v-2h52" fill="#bddadb"/>


        </g>
        </svg>
    )
  }
}
