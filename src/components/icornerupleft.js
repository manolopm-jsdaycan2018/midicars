'use strict'
import React from 'react'

export default class CornerUpLeft extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-692 -174)">
        <path d="m692 189 3.9-0.45q2.1-0.4 3.8-1.2 1.95-0.9 3.35-2.3 1.45-1.4 2.35-3.3v-0.05q0.85-1.65 1.25-3.8l0.35-3.45v-0.45h49v64h-64v-49" fill="#a6c9cb"/>
        <path d="m692 189v-5l2.6-0.3 2.5-0.85h0.05q1.3-0.6 2.2-1.5h0.05q0.95-0.95 1.5-2.2l0.05-0.05 0.8-2.5 0.05-0.1 0.2-2.5h5v0.45l-0.35 3.45q-0.4 2.15-1.25 3.8v0.05q-0.9 1.9-2.35 3.3-1.4 1.4-3.35 2.3-1.7 0.8-3.8 1.2l-3.9 0.45" fill="#9cc0c2"/>
        <path d="m700 174-0.15 1.9-0.05 0.2-0.65 1.95-0.05 0.05-1.2 1.7-0.05 0.05-4.4-4.4h0.05l0.25-0.45h0.05l0.15-0.5 0.05-0.5h6" fill="#e86a17"/>
        <path d="m702 174-0.2 2.5-0.05 0.1-0.8 2.5-0.05 0.05q-0.55 1.25-1.5 2.2h-0.05l-1.5-1.5 0.05-0.05 1.2-1.7 0.05-0.05 0.65-1.95 0.05-0.2 0.15-1.9h2m-8 0-0.05 0.5-0.15 0.5h-0.05l-0.25 0.45h-0.05l-1.45-1.45h2" fill="#db6212"/>
        <path d="m693.45 175.45 4.4 4.4-1.75 1.25-2.05 0.65l-2.05 0.25v-6l0.5-0.05 0.5-0.2h0.05l0.4-0.3" fill="#fafafa"/>
        <path d="m693.45 175.45-0.4 0.3h-0.05l-0.5 0.2-0.5 0.05v-2l1.45 1.45m5.9 5.9q-0.9 0.9-2.2 1.5h-0.05l-2.5 0.85-2.6 0.3v-2l2.05-0.25 2.05-0.65 1.75-1.25 1.5 1.5" fill="#eee"/>
        </g>
        </svg>
    )
  }
}
